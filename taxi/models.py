from django.db import models


class Driver(models.Model):
    driver_name = models.CharField(max_length=255)
    driver_car = models.CharField(max_length=255)
    driver_phone = models.IntegerField()
    driver_seats = models.PositiveIntegerField()
    driver_car_number = models.CharField(max_length=255)


class City(models.Model):
    city_name = models.CharField(verbose_name='city_name', max_length=255)


class Order(models.Model):
    from_city = models.ForeignKey(City, related_name='from_city', on_delete=models.CASCADE)
    to_city = models.ForeignKey(City, related_name='to_city', on_delete=models.CASCADE)
    price = models.PositiveIntegerField()
    order_date = models.DateField()
    order_time = models.TimeField()
    pickup_location = models.CharField(max_length=255)
    passengers_number = models.PositiveIntegerField()
    comment = models.TextField()
